package com.tado.tv.sample

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var context: Context

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        context = this
        initView()
    }

    private fun initView() {
        tv_vertical_video.setOnClickListener(verticalListener)
        tv_horizontal_video.setOnClickListener(horizontalListener)
    }

    private val verticalListener = View.OnClickListener {
        val intent = Intent(context, WebActivity::class.java)
        intent.putExtra("url", "https://botolkecap.tadotv.com/watch/PGKulsG")
        intent.putExtra("isVertical", true)
        startActivity(intent)
    }

    private val horizontalListener = View.OnClickListener {
        val intent = Intent(context, WebActivity::class.java)
        intent.putExtra("url", "https://botolkecap.tadotv.com/watch/makmur")
        intent.putExtra("isVertical", false)
        startActivity(intent)
    }

}
