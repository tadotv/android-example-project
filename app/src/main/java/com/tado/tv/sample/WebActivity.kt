package com.tado.tv.sample

import android.content.Context
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.webkit.WebChromeClient
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_web.*


class WebActivity : AppCompatActivity() {

    private lateinit var context: Context
    private var url: String = ""
    private var isVertical: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web)
        context = this

        getIntentData()
        initView()
    }

    private fun getIntentData(){
        val extra = intent.extras
        if (extra != null) {
            url = extra.getString("url", "")
            isVertical = extra.getBoolean("isVertical", true)
        }
    }

    private fun initView(){
        if(isVertical){
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        }else{
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        }

        if(!url.equals("")){
            webview.webChromeClient = WebChromeClient()
            webview.settings.javaScriptEnabled = true
            webview.settings.domStorageEnabled = true
            webview.loadUrl(url)
        }
    }

}
