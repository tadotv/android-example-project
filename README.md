# Android Sample Project
This is a sample project to embed TADOtv Web Player on android using WebView.

## Getting Started
First you need to create class that use WebView android
```
<WebView
        android:id="@+id/webview"
        android:layout_width="match_parent"
        android:layout_height="match_parent"/>
```

## WebView setup
Setup your webview like this one :

```
webview.webChromeClient = WebChromeClient()
webview.settings.javaScriptEnabled = true
webview.settings.domStorageEnabled = true
webview.loadUrl(url)
```

## Manifest setup
Add internet permission

```
<uses-permission android:name="android.permission.INTERNET" />
```

## Orientation setup
### Vertical orientation
If you want to use vertical video you can call this function on your activity class
```
requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
```

### Horizontal orientation
If you want to use horizontal video you can call this function on your activity class
```
requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
```

## Fullscreen themes
For better experience, we recommend to use a fullscreen themes. You can edit your existing themes on values/styles.xml , then add this code

```
<style name="AppTheme.NoActionBar">
      <item name="windowActionBar">false</item>
      <item name="windowNoTitle">true</item>
</style>
```

